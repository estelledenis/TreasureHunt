<a href="http://dhdm.de:8111/viewType.html?buildTypeId=Geoschnitzel_Build&guest=1">
<img src="http://dhdm.de:8111/app/rest/builds/buildType:(id:Geoschnitzel_Build)/statusIcon"/>
</a>

# Introduction
TreasureHunt is an interactive Android application of a game based on the classic scavenger hunt. It include components such as  including messages, hints, targets, hunts, users, and transactions. It utilizes a database and Google Maps libraries to generate a map, generate a map view and a location for the user in the map. Like with most scavenger hunt games, the goal is to get to the target before the time runs out. Hints can also be found and items purchased along the way to increase likelihood of success.

# Components/Rules

Some components and rules of the game are:

- Area: The generated area of the map the user is located, radius measured in meters.
- Start area will be randomly chosen for the user and the map changes every reset of the game.
- Map: Only part of the map will be viewed by the user at any given time.
- As the user moves, more of the map will be revealed.
- Coordinates: coordinates given to target by a hint.
- Direction: direction given to target by a hint.
- Hints: these give direction to a user on where the target is located.
- Hints are locked until purchased or until a specific amount of time (given by each hint) has elapsed.
- Hints are in the form of an image and text.
- Item inventory: inventory for all items user has obtained.
- The item inventory also includes all hints that the user has unlocked.
- Use keyboard arrows to move about player.
- Speed: there is a capped speed for the user.
- Item inventory: inventory for all items user has obtained.
- Items can be obtained through a currency known as "schnitzi."
- User can see their items and schnitzi balance at any time.
- Target: the goal of the treasure hunt.
- There may be more than one target for the user to achieve in any given game.
- One can reset the game after target is obtained or if time runs out.

# ma_sw_wm_afternoon_07

Repository for MA/SE&amp;WM - Group Afternoon 7

# Environment setup
- Install Lombok plugin for Android Studio
    Annotation Processing - set the only the checkbox
- Calculate task graph
    Uncheck     File->Settings->Build/Execution/Deployment->Compiler->Configure On Demand



